$(document).ready(function () {
    var headerHeight = $(".top-bar").outerHeight();
    $('.scroll').click(function (e) {
        e.preventDefault();

        var linkHref = $(this).attr("href");

        $("html, body").animate({
            scrollTop: $(linkHref).offset().top - headerHeight
        }, 1000);


    });

});
